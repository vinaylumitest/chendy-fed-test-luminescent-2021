module.exports = function(grunt) {
  grunt.initConfig({
    connect: {
        server: {
          options: {
            port: 8080,
            base: './'
          }
        }
      },
      uglify: {
        dist:{
          files: {
            'app/js/index.min.js':'src/scripts/index.js',
            'app/js/form.min.js':'src/scripts/formValidate.js',
          }
        }
      },
    less: {
      development: {
        options: {
          paths: ["assets/css"]
        },
        files: {"app/css/main.css": "src/less/main.less"}
      },
      watch: {
        files: "*.less",
        tasks: ["less"]
      }
    },
    watch: {
      styles:{
        options:{
          livereload: true,
          spawn: false,
          event: ['added','deleted','changed']
        },
        files:['**/*.less'],
        tasks:['less']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', ['connect:server','uglify', 'watch']);
};
