function validateForm() {
  let name = document.forms["myForm"]["name"].value;
  let surname = document.forms["myForm"]["surname"].value;
  let email = document.forms["myForm"]["email"].value;
  let phone = document.forms["myForm"]["phone"].value;
  let password = document.forms["myForm"]["password"].value;

  if (!checkName(name)) {
    alert("Name must not be empty and can contain alphabets");
    return false;
  }

  if (!checkName(surname)) {
    alert("Surname must not be empty and can contain alphabets");
    return false;
  }
  if (!checkEmail(email)) {
    alert("Email is not correct");
    return false;
  }
  if (!checkPassword(password)) {
    alert("Password format is not correct");
    return false;
  }
  if (!checkPhone(phone)) {
    alert("phone number is not correct");
    return false;
  }
  return true;
}

function checkName(name) {
  if (name === "") {
    return false;
  }

  var regex = /^[a-zA-Z ]{2,30}$/;
  return regex.test(name);
}

function checkPhone(phone) {
  if (phone === "") {
    return false;
  }
  var regex = /^\+{0,1}[0-9]{10,12}$/;
  return regex.test(phone);
}

function checkEmail(email) {
  if (email === "") {
    return false;
  }

  var regex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
}

function checkPassword(password) {
  if (password === "") {
    return false;
  }
  var regex = /^(?=.*[@$!%*#?&])(?=.*\d)[A-Za-z\d]{8,}$/;
  return regex.test(password);
}
