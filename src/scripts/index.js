//Function to call product APi List
function productApiFunctionCall() {
  fetch(`/src/data/products.json`, { method: "GET" })
    .then((Result) => Result.json())
    .then((productArray) => {
      let totalRecords = document.getElementById("totalRecords");
      let mainContainer = document.getElementById("productContainer");
      let hTemplate = document.getElementById("product-template").content;
      let starTemplate = document.getElementById("star-template").content;
      let favTemplate = document.getElementById("favTrue").content;
      let favFalseTemplate = document.getElementById("favFalse").content;
      totalRecords.innerHTML = `${productArray.length} results`;
      //console.log(hTemplate,'hTemplate');
      // Printing our response
      if (productArray.length > 0) {
        for (product of productArray) {
          let card = hTemplate.cloneNode(true);
          card.querySelector(".productName").textContent = product.name;
          card.querySelector("#productSize").textContent = product.size;
          card.querySelector("#picImage").src = product.picture;
          card.querySelector("#picSourceSet").srcset = product.picture;
          //Looping for ratingTemplate
          let ratingProduct = product.rating;

          while (ratingProduct > 0) {
            ratingProduct--;
            let starRating = starTemplate.cloneNode(true);
            card.querySelector("#productRating").appendChild(starRating);
          }

          if (product.isFav) {
            let fav = favTemplate.cloneNode(true);
            card.querySelector("#favicon").appendChild(fav);
          } else {
            let favFalse = favFalseTemplate.cloneNode(true);
            card.querySelector("#favicon").appendChild(favFalse);
          }

          card.querySelector("#productNewPrice").textContent = product.price;
          if (product.oldPrice) {
            card.querySelector("#productOldPrice").textContent =
              product.oldPrice;
            card.querySelector(
              "#productSavePrice"
            ).textContent = `You Save ${product.savings}`;
          }

          // card.querySelector("#favIcon").onclick = () => { product.isFav=!product.isFav; };
          // if (todo.data[id][1]) {
          //   row.querySelector(".todo-item").classList.add("todo-ok");
          // }
          mainContainer.appendChild(card);
        }
      }
    })
    .catch((errorMsg) => {
      console.log(errorMsg);
    });
}

function toggleList() {

  let divMain =document.getElementById('productContainer');
   if(divMain.classList.contains('productContainerFlex')){
    divMain.classList.remove("productContainerFlex");
    divMain.classList.add("productContainerBlock");

   } else{
    divMain.classList.remove("productContainerBlock");
    divMain.classList.add("productContainerFlex");
   }
}

window.onload = productApiFunctionCall;
